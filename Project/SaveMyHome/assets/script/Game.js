const CommonPeople = require('./common/commonPeople');
cc.Class({
    extends: cc.Component,

    properties: {
        MaxMonsterCount: 50,
        MonsterList: [],
        bgAudio: {
            default: [],
            type: [cc.AudioClip]
        }
    },

    onLoad() {
        window.Game_Listener = this.node;
        let manager = cc.director.getCollisionManager();
        manager.enabled = true;
        // manager.enabledDebugDraw = true;
        this.PlayerInput = this.node.getComponent('playerInput');
        this.UI = this.node.getComponent('UI');
    },


    start() {
        this.GameStart();
        cc.audioEngine.play(this.bgAudio[Math.floor(Math.random() * this.bgAudio.length)], true);
    },

    GameStart() {
        let self = this;
        window.dad = CommonPeople.SpawnPeople(0, 0);
        window.dad.ObjUI.setPosition(cc.v2(20, 0));
        window.mom = CommonPeople.SpawnPeople(1, 0);
        window.mom.ObjUI.setPosition(cc.v2(-20, 0));

        this.PlayerInput.ChangeCurrentPeople(1);
        this.scheduleOnce(function () {
            window.Game_Listener.emit('people_load_done', function () { });
            self.SpawnMonsterEvent();
        }, 1);

        window.Game_Listener.on('gameover', function () {
            self.UI.viewGameOver();
        })
    },


    SpawnMonsterEvent() {
        let self = this;
        this.schedule(function () {
            if (self.MonsterList.length > self.MaxMonsterCount) {
                return;
            }
            let sMonster = undefined;
            if (Math.random() > 0.5) {
                sMonster = CommonPeople.SpawnPeople(99, 0);
            } else {
                sMonster = CommonPeople.SpawnPeople(98, 0);
            }
            self.MonsterList.push(sMonster);
        }, 5);
    }

});
