

cc.Class({
    extends: cc.Component,

    properties: {
        dadPrefab: {
            default: null,
            type: cc.Prefab
        },
        momPrefab: {
            default: null,
            type: cc.Prefab
        },
        slimePrefab: {
            default: null,
            type: cc.Prefab
        },
        goblinPrefab: {
            default: null,
            type: cc.Prefab
        },
        PeopleNodeLayer: {
            default: null,
            type: cc.Node
        },
        EnemyNodeLayer: {
            default: null,
            type: cc.Node
        },
        golfPrefab: {
            default: null,
            type: cc.Prefab
        },
        WeaponNodeLayer: {
            default: null,
            type: cc.Node
        },
        SpawnEnemyPos: {
            default: null,
            type: cc.Node
        },
        touchTable: {
            default: null,
            type: cc.Node
        },
        momGolfPrefab: {
            default: null,
            type: cc.Prefab
        },
        GameoverWindow: {
            default: null,
            type: cc.Node
        },
        camera: {
            default: null,
            type: cc.Node
        },
        houseLife: {
            default: null,
            type: cc.Node
        },
        MaxHouseLife: 3000,
        CurrentHouseLife: 3000,
    },

    onLoad() {
        this.MinX = -this.node.width / 2;
        this.MaxX = this.node.width / 2;
        this.MinY = -this.node.height / 2;
        this.MaxY = this.node.height / 2;
    },
    start() {
        let self = this;
        this.rate = this.houseLife.width / this.MaxHouseLife;
        let timer_func = function () {
            self.houseLife.width = this.rate * this.CurrentHouseLife;
            if (self.houseLife.width < 1) {
                window.Game_Listener.emit('gameover');
                self.unschedule(timer_func);
            }
        }
        this.schedule(timer_func, 0.2);
    },
    InstantiatePeople(peopleID, peopleDir) {
        let sPeople = cc.instantiate(this.pickCharacterPrefab(peopleID));
        sPeople.name = peopleID + '';
        this.PeopleNodeLayer.addChild(sPeople);
        sPeople.setPosition(cc.v2(0, 0));
        return sPeople;
    },

    RandomPosSpawnNormalEnemy(peopleID, peopleDir) {
        let randomBorderPos = this.calcRandomBorderPos();
        let enemyID = peopleID;
        let sEnemy = cc.instantiate(this.pickCharacterPrefab(enemyID));
        this.EnemyNodeLayer.addChild(sEnemy);
        sEnemy.setPosition(randomBorderPos);
        console.log('sEnemy position', sEnemy.position);
        return sEnemy;
    },

    calcRandomBorderPos() {
        let templength = this.SpawnEnemyPos.children.length;
        let random = Math.floor(Math.random() * templength);
        return this.SpawnEnemyPos.children[random].position;
    },

    pickCharacterPrefab(id) {
        switch (id) {
            case 0: return this.dadPrefab;
            case 1: return this.momPrefab;
            case 99: return this.slimePrefab;
            case 98: return this.goblinPrefab;
        }
    },


    SpawnGolf(dir, position) {
        let self = this;
        let sGolf = cc.instantiate(this.golfPrefab);
        // console.log('UI sGolf');
        let WPos = this.PeopleNodeLayer.convertToWorldSpaceAR(position);
        let NPos = this.WeaponNodeLayer.convertToNodeSpace(WPos);
        this.node.addChild(sGolf);
        sGolf.setPosition(NPos);
        let v2 = undefined;
        // console.log('sGolf.speed ', sGolf.speed);
        let speed = sGolf.speed;
        switch (dir) {
            case 0: v2 = cc.v2(0, -speed); break;
            case 1: v2 = cc.v2(-speed, 0); break;
            case 2: v2 = cc.v2(speed, 0); break;
            case 3: v2 = cc.v2(0, speed); break;
        }
        let flyAct = cc.moveBy(0.3, v2);
        let completeCallback = cc.callFunc(function () {
            if (sGolf != undefined) {
                sGolf.destroy();
            }
        })
        let seq = cc.sequence(flyAct, completeCallback);
        sGolf.runAction(seq);
    },

    SpawnMomGolf(dir, position) {
        let self = this;
        let sGolf = cc.instantiate(this.momGolfPrefab);
        let WPos = this.PeopleNodeLayer.convertToWorldSpaceAR(position);
        let NPos = this.WeaponNodeLayer.convertToNodeSpace(WPos);
        this.node.addChild(sGolf);
        sGolf.setPosition(NPos);
        let v2 = undefined;
        let speed = sGolf.speed;
        switch (dir) {
            case 0: v2 = cc.v2(0, -speed); break;
            case 1: v2 = cc.v2(-speed, 0); break;
            case 2: v2 = cc.v2(speed, 0); break;
            case 3: v2 = cc.v2(0, speed); break;
        }
        let flyAct = cc.moveBy(0.3, v2);
        let completeCallback = cc.callFunc(function () {
            if (sGolf != undefined) {
                sGolf.destroy();
            }
        })
        let seq = cc.sequence(flyAct, completeCallback);
        sGolf.runAction(seq);
    },

    touchMode() {
        this.touchTable.active = !this.touchTable.active;
        this.touchTable.getChildByName('up').on('touchmove', function (event) {
            cc.systemEvent.emit(cc.SystemEvent.EventType.KEY_DOWN, function () {
                event.keyCode = cc.macro.KEY.w;
            });
        });
        this.touchTable.getChildByName('down').on('touchmove', function (event) {
            cc.systemEvent.emit(cc.SystemEvent.EventType.KEY_DOWN, function () {
                event.keyCode = cc.macro.KEY.s;
            });
        });
        this.touchTable.getChildByName('left').on('touchmove', function (event) {
            cc.systemEvent.emit(cc.SystemEvent.EventType.KEY_DOWN, function () {
                event.keyCode = cc.macro.KEY.a;
            });
        });
        this.touchTable.getChildByName('right').on('touchmove', function (event) {
            cc.systemEvent.emit(cc.SystemEvent.EventType.KEY_DOWN, function () {
                event.keyCode = cc.macro.KEY.d;
            });
        });
        this.touchTable.getChildByName('y').on('touchmove', function (event) {
            cc.systemEvent.emit(cc.SystemEvent.EventType.KEY_DOWN, function () {
                event.keyCode = cc.macro.KEY.y;
            });
        });
        this.touchTable.getChildByName('e').on('touchmove', function (event) {
            cc.systemEvent.emit(cc.SystemEvent.EventType.KEY_DOWN, function () {
                event.keyCode = cc.macro.KEY.e;
            });
        });
        this.touchTable.getChildByName('r').on('touchmove', function (event) {
            cc.systemEvent.emit(cc.SystemEvent.EventType.KEY_DOWN, function () {
                event.keyCode = cc.macro.KEY.r;
            });
        });
    },

    viewGameOver() {
        console.log('gameover');
        this.GameoverWindow.active = true;
        this.node.getComponent('playerInput').destroy();
        this.camera.setPosition(cc.v2(0, 0));
    },

    on_ClickBack2open() {
        cc.director.loadScene('first');
    },

    ReduceHouseLife(reduce) {
        this.CurrentHouseLife -= reduce;
    }
});
