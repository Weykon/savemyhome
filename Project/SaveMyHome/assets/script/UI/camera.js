
cc.Class({
    extends: cc.Component,

    properties: {
        PeopleNodeLayer: {
            default: null,
            type: cc.Node
        },
        CameraOffset: {
            default: null,
            type: cc.Node
        }
    },

    CatchPlayerID() {
        this.currentPlayerID = window.GameInfo.Get_CurrentPeople();   //id
        for (let a = 0; a < this.PeopleNodeLayer.children.length; a++) {
            if (this.PeopleNodeLayer.children[a].name.indexOf(this.currentPlayerID + '') != -1) {
                this.PlayerObj = this.PeopleNodeLayer.children[a];
            }
        }
    },

    FollowPlayer() {
        this.node.setPosition(this.PlayerObj.position);
    },

    update() {
        if (this.PlayerObj == undefined) {
            this.CatchPlayerID();
            return;
        }
        this.FollowPlayer();
    },

    lateUpdate() {
        this.ifExceedCameraBorder();
    },

    ifExceedCameraBorder() {
        if (this.node.y >= 315) {
            this.node.y = 315;
        }
        if (this.node.x >= 550) {
            this.node.x = 550;
        }
        if (this.node.x <= -550) {
            this.node.x = -550;
        }
        if (this.node.y <= -300) {
            this.node.y = -300;
        }
    }
});
