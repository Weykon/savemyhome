window.GameInfo = {
    CurrentPeople: undefined,

    GolfDamage: 150,
    momGolfDamage: 300,

    Get_CurrentPeople() {
        if (this.CurrentPeople == undefined) {
            this.CurrentPeople = 0;
        }
        return this.CurrentPeople;
    },

    Set_CurrentPeople(ID) {
        this.CurrentPeople = ID;
    },

}