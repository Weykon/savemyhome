class CommonPeople {
    //  0-Father 1-Mather 2-Son 
    //
    constructor(peopleID, peopleDir, peopleState) {
        this.peopleID = peopleID;
        this.peopleDir = peopleDir;
        this.peopleState = peopleState;
        this.ObjUI = this.Instantiate();
        this.ObjAnimation = this.ObjUI.getComponent(cc.Animation);
        console.log('new people done', peopleID, peopleDir, peopleState);
    }

    Instantiate() {
        let UI = cc.find('Canvas').getComponent('UI');
        if (this.peopleID < 50) {
            let peopleObj = UI.InstantiatePeople(this.peopleID, this.peopleDir);
            return peopleObj;
        } else {
            console.log('spawn enemy')
            let enemyObj = UI.RandomPosSpawnNormalEnemy(this.peopleID, this.peopleDir);
            return enemyObj;
        }
    }

    Move(dir, speed) {
        // console.log('CommonPeople: Move');
        this.touchID = dir;
        let x = 0;
        let y = 0;
        switch (dir) {
            case 0: y = -1; break;
            case 1: x = -1; break;
            case 2: x = 1; break;
            case 3: y = 1; break;
        }
        this.ObjUI.x += x * speed;
        this.ObjUI.y += y * speed;
    }

    Animation(dir) {
        if (this.dir != undefined && this.dir == dir) {
            return;  //避免行进当中多次播放
        }
        this.dir = dir;
        switch (dir) {
            case 0: this.ObjAnimation.play('moveDown'); break;
            case 1: this.ObjAnimation.play('moveLeft'); break;
            case 2: this.ObjAnimation.play('moveRight'); break;
            case 3: this.ObjAnimation.play('moveUp'); break;
        }
    }
    StopAnimation(dir) {
        switch (dir) {
            case 0: this.ObjAnimation.stop('moveDown'); break;
            case 1: this.ObjAnimation.stop('moveLeft'); break;
            case 2: this.ObjAnimation.stop('moveRight'); break;
            case 3: this.ObjAnimation.stop('moveUp'); break;
        }
        this.dir = dir;
    }

    Attack(dir) {
        switch (dir) {
            case 0: this.ObjAnimation.play('frontAttack'); break;
            case 1: this.ObjAnimation.play('leftAttack'); break;
            case 2: this.ObjAnimation.play('rightAttack'); break;
            case 3: this.ObjAnimation.play('backAttack'); break;
        }
    }
}


module.exports = {
    SpawnPeople: function (peopleID, peopleDir) {
        let sPeople = new CommonPeople(peopleID, peopleDir);
        return sPeople;
    }
}