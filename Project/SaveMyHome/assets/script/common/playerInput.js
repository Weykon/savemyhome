
cc.Class({
    extends: cc.Component,

    properties: {
        PeopleNodeLayer: {
            default: null,
            type: cc.Node
        },
        peopleList: [],
        Camera: {
            default: null,
            type: cc.Node
        }
    },

    onLoad() {
        let self = this;
        window.Game_Listener.on('people_load_done', function () {
            console.log('接受到people_load_done');
            for (let a = 0; a < self.PeopleNodeLayer.children.length; a++) {
                self.peopleList.push(self.PeopleNodeLayer.children[a]);
            }
            self.on_KeyListen();
        })
    },

    onDestroy() {
        this.off_KeyListen();
    },

    start() {
        this.currentPeople = window.GameInfo.Get_CurrentPeople();
    },

    on_KeyListen() {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    },

    off_KeyListen() {
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    },

    onKeyUp(event) {
        // this.isOnClick = false;
        switch (event.keyCode) {
            case cc.macro.KEY.a:
                // cc.log('turn up a');
                this.peopleList[this.currentPeople].jsComponent.StopAction(1);
                break;
            case cc.macro.KEY.d:
                // cc.log('turn up d');
                this.peopleList[this.currentPeople].jsComponent.StopAction(2);
                break;
            case cc.macro.KEY.w:
                // cc.log('turn up w');
                this.peopleList[this.currentPeople].jsComponent.StopAction(3);
                break;
            case cc.macro.KEY.s:
                // cc.log('turn up s');
                this.peopleList[this.currentPeople].jsComponent.StopAction(0);
                break;
            case cc.macro.KEY.y:
                // cc.log('turn up y');
                this.peopleList[this.currentPeople].jsComponent.StopAction();
                break;
        }
    },

    onKeyDown(event) {
        // if (this.isOnClick == true) {
        //     return;
        // }
        // this.isOnClick = true;
        switch (event.keyCode) {
            case cc.macro.KEY.a:
                // cc.log('turn down a');
                this.peopleList[this.currentPeople].jsComponent.Move(1);
                break;
            case cc.macro.KEY.d:
                // cc.log('turn down d');
                this.peopleList[this.currentPeople].jsComponent.Move(2);
                break;
            case cc.macro.KEY.w:
                // cc.log('turn down w');
                this.peopleList[this.currentPeople].jsComponent.Move(3);
                break;
            case cc.macro.KEY.s:
                // cc.log('turn down s');
                this.peopleList[this.currentPeople].jsComponent.Move(0);
                break;
            case cc.macro.KEY.y:
                // cc.log('turn down y');
                this.peopleList[this.currentPeople].jsComponent.Attack();
                break;
            case cc.macro.KEY.e:
                // cc.log('turn down e');
                this.ChangeCurrentPeople(0);
                break;
            case cc.macro.KEY.r:
                // cc.log('turn down r');
                this.ChangeCurrentPeople(1);
                break;
        }
    },

    ChangeCurrentPeople(ID) {
        this.currentPeople = ID;
        window.GameInfo.Set_CurrentPeople(ID);
        this.Camera.getComponent('camera').PlayerObj = undefined;
    }

});
