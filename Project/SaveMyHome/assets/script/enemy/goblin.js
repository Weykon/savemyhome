
cc.Class({
    extends: cc.Component,

    properties: {
        MaxLife: 400,
    },

    start() {
        this.node.jsComponent = this;
        this.Animation = this.node.getComponent(cc.Animation);
        this.spriteFrame = this.node.getComponent(cc.Sprite).spriteFrame;
        this.AudioSource = this.node.getComponent(cc.AudioSource);

        let move = cc.moveTo(10, cc.v2(0, 0));
        this.node.runAction(move);
        if (this.node.x > 0) {
            this.Animation.play('moveLeft');
        } else {
            this.Animation.play('moveRight');
        }
    },

    onCollisionEnter: function (other, self) {
        if (other.tag == 50) {
            this.MaxLife -= window.GameInfo.GolfDamage;
            if (this.node.x > 0) {
                this.Animation.play('hurtLeft');
            } else {
                this.Animation.play('hurtRight');
            }
            if (this.MaxLife <= 0) {
                this.node.stopAllActions();
                this.node.destroy();
            }
            return;
        }
        if (other.tag == 51) {
            this.MaxLife -= window.GameInfo.momGolfDamage;
            if (this.node.x > 0) {
                this.Animation.play('hurtLeft');
            } else {
                this.Animation.play('hurtRight');
            }
            if (this.MaxLife <= 0) {
                this.node.stopAllActions();
                this.node.destroy();
            }
            return;
        }
        if (other.tag == 30) {
            this.attackHouse();
            return;
        }
        if (other.tag == 1) {
            this.attackPlayer(other);
        }
        this.node.stopAllActions();
    },

    onCollisionExit: function (other, self) {
        this.isStay = false;
        if (other.tag == 50) {
            this.moveBack();
        }
    },

    onCollisionStay: function () {
        this.isStay = true;
    },

    attackPlayer(other) {
        let self = this;
        this.Animation.play('attack');
        let attack_func = function () {
            if (self.isStay) {
                other.node.jsComponent.ReduceCurrentLife(50);
            } else {
                self.unschedule(attack_func);
            }
        }
        this.schedule(attack_func, 0.6);
    },

    attackHouse() {
        let self = this;
        let UI = cc.find('Canvas').getComponent('UI');
        this.Animation.play('attack');
        let attack_func = function () {
            if (self.isStay) {
                UI.ReduceHouseLife(100);
            } else {
                self.unschedule(attack_func);
            }
        }
        this.schedule(attack_func, 0.6);
    },

    moveToHouse() {
        let move = cc.moveTo(10, cc.v2(0, 0));
        this.node.runAction(move);
        this.Animation.play('walk');
    },

    moveBack() {
        let self = this;
        let originPos = this.node.position;
        let moveBackAct = cc.moveBy(0.5, cc.v2(originPos.x / 5, originPos.y / 5));
        let completeCallback = cc.callFunc(self.moveToHouse, this);
        let seq = cc.sequence(moveBackAct, completeCallback);
        this.node.runAction(seq);
    }
});
