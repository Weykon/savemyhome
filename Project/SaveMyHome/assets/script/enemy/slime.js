const CommonPeople = require('../common/commonPeople');
cc.Class({
    extends: cc.Component,

    properties: {
        MaxLife: 200,
    },

    start() {
        this.node.jsComponent = this;
        this.Animation = this.node.getComponent(cc.Animation);
        this.spriteFrame = this.node.getComponent(cc.Sprite).spriteFrame;
        this.AudioSource = this.node.getComponent(cc.AudioSource);

        let move = cc.moveTo(10, cc.v2(0, 0));
        this.node.runAction(move);
        this.Animation.play('walk');
    },

    onCollisionEnter: function (other, self) {
        if (other.tag == 50) {
            this.MaxLife -= window.GameInfo.GolfDamage;
            this.Animation.play('hurt');
            if (this.MaxLife <= 0) {
                this.node.stopAllActions();
                this.node.destroy();
            }
            return;
        }
        if (other.tag == 51) {
            this.MaxLife -= window.GameInfo.momGolfDamage;
            this.Animation.play('hurt');
            if (this.MaxLife <= 0) {
                this.node.stopAllActions();
                this.node.destroy();
            }
            return;
        }
        if (other.tag == 30) {
            this.attackHouse();
            return;
        }
        if (other.tag == 1) {
            this.attackPlayer(other);
        }
        this.node.stopAllActions();
    },

    onCollisionExit: function (other, self) {
        if (other.tag == 50) {
            this.moveBack();
        }
    },

    attackPlayer(other) {
        other.node.jsComponent.ReduceCurrentLife(50);
    },


    attackHouse() {
        let self = this;
        let UI = cc.find('Canvas').getComponent('UI');
        this.Animation.play('attack');
        let attack_func = function () {
            if (self.isStay) {
                UI.ReduceHouseLife(50);
            } else {
                self.unschedule(attack_func);
            }
        }
        this.schedule(attack_func, 0.6);
    },


    moveToHouse() {
        let move = cc.moveTo(10, cc.v2(0, 0));
        this.node.runAction(move);
        this.Animation.play('walk');
    },

    moveBack() {
        let self = this;
        let originPos = this.node.position;
        let moveBackAct = cc.moveBy(0.5, cc.v2(originPos.x / 5, originPos.y / 5));
        let completeCallback = cc.callFunc(self.moveToHouse, this);
        let seq = cc.sequence(moveBackAct, completeCallback);
        this.node.runAction(seq);
    }
});
