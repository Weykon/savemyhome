const CommonPeople = require('../common/commonPeople');

cc.Class({
    extends: cc.Component,

    properties: {
        m_spriteFrameList: {
            default: [],
            type: [cc.SpriteFrame]
        },
        moveSpeed: 5,
        MaxLife: 1000,
        CurrentLife: 1000,
    },

    start() {
        let self = this;
        this.node.jsComponent = this;
        this.Animation = this.node.getComponent(cc.Animation);
        this.node.lifeUI = this.node.getChildByName('life');
        this.node.lifeUI.zIndex = 99;
        this.rate = this.node.lifeUI.width / this.MaxLife;
        this.spriteFrame = this.node.getComponent(cc.Sprite).spriteFrame;
        this.AudioSource = this.node.getComponent(cc.AudioSource);
        this.UI = cc.find('Canvas').getComponent('UI');

        let timer_func = function () {
            self.node.lifeUI.width = this.rate * this.CurrentLife;
            if (self.node.lifeUI.width < 1) {
                window.Game_Listener.emit('gameover');
                self.unschedule(timer_func);
            }
        }
        this.schedule(timer_func, 0.2);
    },

    Attack() {
        let self = this;

        if (this.CanNotAttack == true) {
            return;
        }
        this.CanNotAttack = true;
        let CD = 0.3;
        let one_part = function () {
            CD -= 0.1;
            if (CD <= 0) {
                self.CanNotAttack = false;
                self.unschedule(one_part);
            }
        }
        this.schedule(one_part, 0.1);
        if (this.isMove) {
            window.mom.StopAnimation(this.dir);
        }
        window.mom.Attack(this.dir);
        this.AudioSource.play('momAttack');
        this.adc = true;
        if (this.adc) {
            this.UI.SpawnMomGolf(this.dir, this.node.position);
        }
    },

    Move(dir) {
        this.touchID = dir;
        this.dir = dir;
        this.node.dir = dir;
        this.isMove = true;
        window.mom.Animation(this.dir);
    },

    StopAction(dir) {
        if (this.touchID == dir) {
            this.isMove = false;
        }
        window.mom.StopAnimation(dir);

        this.spriteFrame = this.m_spriteFrameList[dir];
    },

    update(dt) {
        if (this.isMove) {
            if (this.StayWall) {
                window.mom.Move(this.backDir, this.moveSpeed);
            }
            window.mom.Move(this.dir, this.moveSpeed);
        }
    },

    onCollisionEnter: function (other, self) {
        if (other.tag == 99) {
            return;
        }
        this.isMove = false;
    },

    onCollisionStay: function (other, self) {
        if (other.tag == 99) {
            return;
        }
        this.StayWall = true;
        let otherPosition = other.node.position;
        let selfPosition = self.node.position;
        let MoveDir = self.node.dir;
        switch (MoveDir) {
            case 0: if (otherPosition.y < selfPosition.y) {
                this.isMove = false;
                this.backDir = 3;
                break;
            } else {
                this.isMove = true;
                break;
            }
            case 1: if (otherPosition.x < selfPosition.x) {
                this.isMove = false;
                this.backDir = 2;
                break;
            } else {
                this.isMove = true;
                break;
            }
            case 2: if (otherPosition.x >= selfPosition.x) {
                this.isMove = false;
                this.backDir = 1;
                break;
            } else {
                this.isMove = true;
                break;
            }
            case 3: if (otherPosition.y >= selfPosition.y) {
                this.isMove = false;
                this.backDir = 0;
                break;
            } else {
                this.isMove = true;
                break;
            }
        }
    },
    onCollisionExit: function (other, self) {
        if (other.tag == 99) {
            return;
        }
        this.backDir = undefined;
        this.StayWall = false;
    },

    ReduceCurrentLife(reduce) {
        this.CurrentLife -= reduce;
    }
});
