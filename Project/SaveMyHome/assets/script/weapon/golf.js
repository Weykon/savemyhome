

cc.Class({
    extends: cc.Component,

    properties: {
        dir: undefined,
        position: undefined,
        speed: 400,
    },

    onLoad() {
        this.node.speed = this.speed;
    },

    onCollisionEnter: function (other, self) {
        this.node.destroy();
    },

});
